![logo.png](https://bitbucket.org/repo/yE6ax8/images/1546716726-logo.png)

# README #

### What is this repository for? ###

This repository is the replication of the private repository made for ECE651 group project for Fall - 2016 at University of Waterloo, under Prof. Werner Dietl.

Project group: Group 2
Project title: Connektor

Members:

* Atishay Goyal | a34goyal
* Diya Burman | d2burman
* Muhammad Ali Ikram | maikram
* Robert Culbert | rjculber


### How do I get set up? ###

1. Setup the database by importing connektor.sql into mySql
2. Provide your database credentials in /server/db/connect.php
3. Provide redirection URL for unauthorized access in /server/authorize/auth.php
4. Put the application code at on an Apache server which can run PHP
5. Navigate to connektor in your browser!